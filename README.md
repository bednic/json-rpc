# JSON RPC 2.0 Implementation

## JSON RPC Docs
[JSON-RPC](https://www.jsonrpc.org/)


## Usage

1. Create controller implementing ProcedureController
2. Pass the controller to dispatcher
3. Handle json request data with `Dispatcher::dispatch(string $data)`
4. Fetch results by calling `Dispatcher::result()`

```php

class MyController implements \JSONRPC\ProcedureController {

    public function subtract(int $a, int $b){
        return $a - $b;
    }
}

$app->post('/rpc', function($request){
    $dispatcher = new \JSONRPC\Dispatcher(new \App\Controller\MyController());
    $request->getBody();
    $response = $dispatcher->dispatch(json_encode($req));
    $result = $response->getResult();
    return json_encode($result);
});

```
## Example

See [tests](tests) in this repo.
