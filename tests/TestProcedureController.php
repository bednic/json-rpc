<?php

declare(strict_types=1);

namespace JSONRPC\Test;

use JSONRPC\ProcedureController;

class TestProcedureController implements ProcedureController
{
    public function subtract(int $minuend, int $subtrahend): int
    {
        return $minuend - $subtrahend;
    }

    public function update(): void
    {
    }

    public function foobar(): void
    {
    }

    //phpcs:ignore
    public function get_data(): array
    {
        return ['hello', 5];
    }

    public function sum($x, $y, $z)
    {
        return $x + $y + $z;
    }

    //phpcs:ignore
    public function notify_hello($arg) { }
}
