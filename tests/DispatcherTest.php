<?php

declare(strict_types=1);

namespace JSONRPC\Test;

use JSONRPC\Dispatcher;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class DispatcherTest extends TestCase
{
    /**
     * @return void
     */
    #[DataProvider('getTestCases')]
    public function testDispatch($req, $res)
    {
        $res        = is_null($res) ?: json_encode(json_decode($res));
        $dispatcher = new Dispatcher(new TestProcedureController());
        $result     = json_encode($dispatcher->dispatch($req));
        $this->assertEquals($res, $result);
    }

    public static function getTestCases(): array
    {
        //phpcs:disable
        return [
            '1: rpc call with positional parameters'             => [
                '{"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1}',
                '{"jsonrpc": "2.0", "result": 19, "id": 1}',
                'rpc call with positional parameters'
            ],
            '2: rpc call with positional parameters'             => [
                '{"jsonrpc": "2.0", "method": "subtract", "params": [23, 42], "id": 2}',
                '{"jsonrpc": "2.0", "result": -19, "id": 2}'
            ],
            '3: rpc call with named parameters'                  => [
                '{"jsonrpc": "2.0", "method": "subtract", "params": {"subtrahend": 23, "minuend": 42}, "id": 3}',
                '{"jsonrpc": "2.0", "result": 19, "id": 3}',

            ],
            '4: rpc call with named parameters'                  => [
                '{"jsonrpc": "2.0", "method": "subtract", "params": {"minuend": 42, "subtrahend": 23}, "id": 4}',
                '{"jsonrpc": "2.0", "result": 19, "id": 4}',
                'rpc call with named parameters'
            ],
            '5: a Notification'                                  => [
                '{"jsonrpc": "2.0", "method": "update", "params": [1,2,3,4,5]}',
                null,
            ],
            '6: a Notification'                                  => [
                '{"jsonrpc": "2.0", "method": "foobar"}',
                null,
            ],
            '7: rpc call of non-existent method'                 => [
                '{"jsonrpc": "2.0", "method": "foo", "id": "1"}',
                '{"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found","data":{"detail":"Method foo does not exist on provided controller."}}, "id": "1"}'
            ],
            '8: rpc call with invalid JSON:'                     => [
                '{"jsonrpc": "2.0", "method": "foobar, "params": "bar", "baz]',
                '{"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error","data":{"detail":"Syntax error"}}, "id": null}'
            ],
            '9: rpc call with invalid Request object'            => [
                '{"jsonrpc": "2.0", "method": 1, "params": "bar"}',
                '{"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request","data":{"detail":"Expected jsonrpc request, but one or more required properties [method, jsonrpc] are missing."}}, "id": null}'
            ],
            '10: rpc call Batch, invalid JSON'                   => [
                '[
                {"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"},
                {"jsonrpc": "2.0", "method"
                ]',
                '{"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error","data":{"detail":"Syntax error"}}, "id": null}'
            ],
            '11: rpc call with an empty Array:'                  => [
                '[]',
                '{"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request","data":{"detail":"Expected non empty json."}}, "id": null}'
            ],
            '12: rpc call with an invalid Batch (but not empty)' => [
                '[1]',
                '[{"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request","data":{"detail":"Expected object, got integer"}}, "id": null}]'
            ],
            '13: rpc call with invalid Batch'                    => [
                '[1,2,3]',
                '[
                {"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":{"detail":"Expected object, got integer"}},"id":null},
                {"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":{"detail":"Expected object, got integer"}},"id":null},
                {"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":{"detail":"Expected object, got integer"}},"id":null}
                ]'
            ],
            '14: rpc call Batch'                                 => [
                '[
        {"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"},
        {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]},
        {"jsonrpc": "2.0", "method": "subtract", "params": [42,23], "id": "2"},
        {"foo": "boo"},
        {"jsonrpc": "2.0", "method": "foo.get", "params": {"name": "myself"}, "id": "5"},
        {"jsonrpc": "2.0", "method": "get_data", "id": "9"}
    ]',
                '[
                {"jsonrpc":"2.0","result":7,"id":"1"},
                {"jsonrpc":"2.0","result":19,"id":"2"},
                {"jsonrpc":"2.0","error":{"code":-32600,"message":"Invalid Request","data":{"detail":"Expected jsonrpc request, but one or more required properties [method, jsonrpc] are missing."}},"id":null},
                {"jsonrpc":"2.0","error":{"code":-32601,"message":"Method not found","data":{"detail":"Method foo.get does not exist on provided controller."}},"id":"5"},
                {"jsonrpc":"2.0","result":["hello",5],"id":"9"}]'
            ],
            '15: rpc call Batch (all notifications)'             => [
                '[
        {"jsonrpc": "2.0", "method": "notify_sum", "params": [1,2,4]},
        {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]}
    ]',
                null
            ]
        ];
        // phpcs:enable
    }
}
