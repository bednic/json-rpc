# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.3.0](https://gitlab.com/bednic/json-rpc/compare/3.2.1...3.3.0) (2023-04-20)


### Added

* Add ProcedureException interface ([263dd83](https://gitlab.com/bednic/json-rpc/commit/263dd83f78cc301f872244f002f2e59bc32698c4))

## [3.2.1](https://gitlab.com/bednic/json-rpc/compare/3.2.0...3.2.1) (2023-04-17)


### Fixed

* on application fail return exception code and message ([31f62cd](https://gitlab.com/bednic/json-rpc/commit/31f62cd043a9b038ca2c4ebc430f5afa8d95a8a1))

## [3.2.0](https://gitlab.com/bednic/json-rpc/compare/3.1.0...3.2.0) (2022-11-23)


### Added

* Add RPCException ([c6abb51](https://gitlab.com/bednic/json-rpc/commit/c6abb51cf85f47300fc8c7a24a4d8f682ff34102))

## [3.1.0](https://gitlab.com/bednic/json-rpc/compare/3.0.0...3.1.0) (2022-11-18)


### Added

* Request is JsonSerializable ([e30a4cf](https://gitlab.com/bednic/json-rpc/commit/e30a4cf6134e22c30b270c29225a703c0c61c2b0))

## [3.0.0](https://gitlab.com/bednic/json-rpc/compare/2.0.0...3.0.0) (2022-11-16)


### ⚠ BREAKING CHANGES

* Remove of method ::result()
Change ::dispatch() return
signature

### Changed

* remove unnecessary method ::result() ([a127118](https://gitlab.com/bednic/json-rpc/commit/a127118064c54bcb305dc346115214df91bbd480))

## [2.0.0](https://gitlab.com/bednic/json-rpc/compare/2.0.0-rc1.0...2.0.0) (2022-11-16)


### ⚠ BREAKING CHANGES


### Added

* add request id to params context ([6d28951](https://gitlab.com/bednic/json-rpc/commit/6d289519e2bd0c65b56a2beea5d16d337e9999f6))


### Changed

* Complete change of behaviour. Test driven. Usage of PHP native call_user_func_array mechanism. Dispatcher accepts new ProcedureController interface, which hold public methods for RPC. ([5e7dd4f](https://gitlab.com/bednic/json-rpc/commit/5e7dd4fd6892e8272b7755c583401c39ebf89a9f))
* parse now throws RPCException/InternalError instead of JsonException ([03e65b1](https://gitlab.com/bednic/json-rpc/commit/03e65b1cae13d043a3c3330028d680883e91b7da))

## [2.0.0-rc1.0](https://gitlab.com/bednic/json-rpc/compare/0.5.1...2.0.0-rc1.0) (2022-09-12)


### ⚠ BREAKING CHANGES

* Procedure::execute

### Changed

* add [@throws](https://gitlab.com/throws) to Procedure interface ([957a8a1](https://gitlab.com/bednic/json-rpc/commit/957a8a1ab5b57c0b88841ae063741006dc3d5a6f)), closes [#5](https://gitlab.com/bednic/json-rpc/issues/5)
* Change internal behavior ([c9c9063](https://gitlab.com/bednic/json-rpc/commit/c9c9063b61b44241de75d4d16d046f701fbf20fa))


### Fixed

* add missing return ([6cb9e7d](https://gitlab.com/bednic/json-rpc/commit/6cb9e7d1d40e28083d2388953df9915b6575051b))

## [1.0.0](https://gitlab.com/bednic/json-rpc/compare/0.5.2...1.0.0) (2022-05-18)


### Changed

* add [@throws](https://gitlab.com/throws) to Procedure interface ([957a8a1](https://gitlab.com/bednic/json-rpc/commit/957a8a1ab5b57c0b88841ae063741006dc3d5a6f)), closes [#5](https://gitlab.com/bednic/json-rpc/issues/5)

### [0.5.1](https://gitlab.com/bednic/json-rpc/compare/0.5.0...0.5.1) (2022-02-09)


### Added

* Implement Stringable to Response/Error ([fa2b08d](https://gitlab.com/bednic/json-rpc/commit/fa2b08dd112bc71fe00f77257461776e70e58a98)), closes [#3](https://gitlab.com/bednic/json-rpc/issues/3)


### Fixed

* Fix jsonDeserialize issue ([dc8e911](https://gitlab.com/bednic/json-rpc/commit/dc8e9114b364b5737031954df5da91bf7c313145)), closes [#2](https://gitlab.com/bednic/json-rpc/issues/2)
