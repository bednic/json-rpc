<?php

declare(strict_types=1);

namespace JSONRPC\Exception;

use Exception;

/**
 * Base class for implementation defined errors
 * @deprecated Use JSONRPC\ProcedureException instead
 * @todo remove in v4
 */
abstract class RPCException extends Exception
{
    /**
     * @var mixed|null
     */
    private mixed $data;

    /**
     * @param int        $code
     * @param string     $message
     * @param mixed|null $data
     */
    public function __construct(int $code, string $message, mixed $data = null)
    {
        parent::__construct($message, $code);
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}
