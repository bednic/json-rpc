<?php

declare(strict_types=1);

namespace JSONRPC;

/**
 * Class Message
 *
 * @package JSONRPC
 */
abstract class Message
{
    public const VERSION      = '2.0';
    public const CONTENT_TYPE = 'application/json';

    /**
     * @var null|string|int
     */
    private string|int|null $id = null;

    /**
     * @return int|string|null
     */
    public function getId(): int|string|null
    {
        return $this->id;
    }

    /**
     * @param int|string|null $id
     */
    protected function setId(int|string|null $id): void
    {
        $this->id = $id;
    }
}
