<?php

declare(strict_types=1);

namespace JSONRPC\Response;

use JsonSerializable;

/**
 * Class Error
 *
 * @package JSONRPC
 */
class Error implements JsonSerializable
{
    /**
     * @var int
     */
    private int $code;

    /**
     * @var string
     */
    private string $message;

    /**
     * @var mixed
     */
    private mixed $data;

    /**
     * Error constructor.
     *
     * @param int $code
     * @param string $message
     * @param mixed $data
     */
    public function __construct(int $code = -32000, string $message = 'Server error', mixed $data = null)
    {
        $this->code    = $code;
        $this->message = $message;
        $this->data    = $data;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    public static function internalError(): self
    {
        return new self(-32603, 'Internal error');
    }

    public static function invalidParams(string $detail): self
    {
        return new self(-32602, "Invalid params", ['detail' => $detail]);
    }

    public static function invalidRequest(string $detail): self
    {
        return new self(-32600, 'Invalid Request', ['detail' => $detail]);
    }

    public static function methodNotFound(string $detail): self
    {
        return new self(-32601, 'Method not found', ['detail' => $detail]);
    }

    public static function parseError(string $detail): self
    {
        return new self(-32700, 'Parse error', ['detail' => $detail]);
    }

    public static function serverError(mixed $data): self
    {
        return new self(-32000, 'Server error', $data);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        $ret = [
            'code'    => $this->getCode(),
            'message' => $this->getMessage(),

        ];
        if ($this->getData()) {
            $ret['data'] = $this->getData();
        }
        return $ret;
    }
}
