<?php

declare(strict_types=1);

namespace JSONRPC\Response;

use JsonSerializable;

/**
 * Class Result
 *
 * @package JSONRPC\Response
 */
class Result implements JsonSerializable
{
    private mixed $value;

    /**
     * Result constructor.
     *
     * @param mixed $data resource is not allowed data type
     */
    public function __construct(mixed $data = null)
    {
        $this->value = $data;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize(): mixed
    {
        return $this->value;
    }
}
