<?php

declare(strict_types=1);

namespace JSONRPC;

use JSONRPC\Response\Error;
use JSONRPC\Response\Result;
use JsonSerializable;

/**
 * Class Request
 *
 * @package JSONRPC
 */
class Request extends Message implements JsonSerializable
{
    /**
     * @var string
     */
    private string $method;

    /**
     * @var array
     */
    private array $params;

    /**
     * Request constructor.
     *
     * @param string          $method
     * @param string|int|null $id
     * @param array           $params
     */
    public function __construct(string $method, string|int|null $id = null, array $params = [])
    {
        $this->method = $method;
        $this->setId($id);
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return bool
     */
    public function isNotification(): bool
    {
        return is_null($this->getId());
    }

    /**
     * @param Result $result
     *
     * @return Response
     */
    public function reply(Result $result): Response
    {
        return new Response($this->getId(), $result);
    }

    /**
     * @param Error $error
     *
     * @return Response
     */
    public function fail(Error $error): Response
    {
        return new Response($this->getId(), $error);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $ret            = [];
        $ret['jsonrpc'] = self::VERSION;
        $ret['method']  = $this->getMethod();
        if (!empty($this->getParams())) {
            $ret['params'] = $this->getParams();
        }
        $ret['id'] = $this->getId();
        return $ret;
    }
}
