<?php

declare(strict_types=1);

namespace JSONRPC;

use ArgumentCountError;
use Exception;
use JsonException;
use JSONRPC\Exception\RPCException;
use JSONRPC\Response\Error;
use JSONRPC\Response\Result;
use stdClass;
use TypeError;

/**
 * Class Dispatcher
 *
 * @package JSONRPC
 */
class Dispatcher
{
    private ProcedureController $controller;
    private array $responses = [];
    private bool $isBatch = false;

    /**
     * @param ProcedureController $controller
     */
    public function __construct(ProcedureController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param string $data
     *
     * @return Response|Response[]|null
     */
    public function dispatch(string $data): Response|array|null
    {
        $this->responses = [];
        try {
            $json = json_decode($data, false, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->error(Error::parseError($e->getMessage()));
            return $this->result();
        }
        if (empty($json)) {
            $this->error(Error::invalidRequest("Expected non empty json."));
            return $this->result();
        }
        if (is_array($json)) {
            $this->isBatch = true;
            foreach ($json as $cmd) {
                if (is_object($cmd)) {
                    $this->validate($cmd);
                } else {
                    $this->error(Error::invalidRequest("Expected object, got " . gettype($cmd)));
                }
            }
        } else {
            $this->validate($json);
        }
        return $this->result();
    }

    /**
     * @param stdClass $json
     *
     * @return void
     */
    private function validate(stdClass $json): void
    {
        if (
            (!property_exists($json, 'method') || !is_string($json->method)) ||
            (!property_exists($json, 'jsonrpc') || $json->jsonrpc !== "2.0")
        ) {
            $this->error(
                Error::invalidRequest(
                    "Expected jsonrpc request, but one or more required properties [method, jsonrpc] are missing."
                )
            );
            return;
        }
        $method = $json->method;
        $id     = property_exists($json, 'id') ? $json->id : null;
        $params = property_exists($json, 'params') ? (array)$json->params : [];
        $this->handle(new Request($method, $id, $params));
    }

    /**
     * @param Request $request
     *
     * @return void
     */
    private function handle(Request $request): void
    {
        $method = $request->getMethod();
        if (!method_exists($this->controller, $request->getMethod())) {
            $this->pushResponse(
                $request->fail(
                    Error::methodNotFound(
                        "Method $method does not exist on provided controller."
                    )
                )
            );
            return;
        }
        try {
            $result = new Result(
                call_user_func_array([$this->controller, $method], $request->getParams())
            );
            if (!$request->isNotification()) {
                $this->pushResponse($request->reply($result));
            }
        } catch (ArgumentCountError | TypeError $e) {
            $this->pushResponse($request->fail(Error::invalidParams($e->getMessage())));
        } catch (RPCException | ProcedureException $exception) {
            if ($exception->getCode() <= -32000 && $exception->getCode() >= -32768) {
                trigger_error(
                    "Error code {$exception->getCode()} is in reserved range [-32768, -32000].
                     See https://www.jsonrpc.org/specification#error_object .",
                    E_USER_WARNING
                );
            }
            $this->pushResponse(
                $request->fail(new Error($exception->getCode(), $exception->getMessage(), $exception->getData()))
            );
        }
    }

    /**
     * @param Error $error
     *
     * @return void
     */
    private function error(Error $error): void
    {
        $this->pushResponse(new Response(null, $error));
    }

    /**
     * @param Response $response
     *
     * @return void
     */
    private function pushResponse(Response $response): void
    {
        $this->responses[] = $response;
    }

    /**
     * @return Response|array|null
     */
    private function result(): Response|array|null
    {
        if (empty($this->responses)) {
            return null;
        } elseif ($this->isBatch) {
            return $this->responses;
        } else {
            return $this->responses[0];
        }
    }
}
