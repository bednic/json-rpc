<?php

declare(strict_types=1);

namespace JSONRPC;

use JsonException;
use JSONRPC\Response\Error;
use JSONRPC\Response\Result;
use JsonSerializable;

/**
 * Class Response
 *
 * @package JSONRPC
 */
class Response extends Message implements JsonSerializable
{
    /**
     * @var Result
     */
    private Result $result;
    /**
     * @var Error|null
     */
    private ?Error $error = null;

    /**
     * Response constructor.
     *
     * @param int|string|null $id
     * @param Error|Result    $result
     */
    public function __construct(int|string|null $id, Result|Error $result)
    {
        if ($result instanceof Error) {
            $this->error = $result;
        } else {
            $this->result = $result;
        }
        $this->setId($id);
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        return $this->result;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        $ret = ['jsonrpc' => Message::VERSION];
        if (!is_null($this->getError())) {
            $ret['error'] = $this->getError();
        } else {
            $ret['result'] = $this->getResult();
        }
        $ret['id'] = $this->getId();
        return $ret;
    }
}
