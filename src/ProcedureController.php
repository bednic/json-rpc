<?php

declare(strict_types=1);

namespace JSONRPC;

/**
 * Marks controller as RPC compatible
 *
 * If you want to throw exception and propagate it to response you have to user JSONRPC\ProcedureException interface
 * for your exception class. Others exceptions will not be handled by dispatcher and passes up.
 */
interface ProcedureController
{
}
