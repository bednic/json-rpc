<?php

/**
 * Created by tomas
 * at 20.04.2023
 */

declare(strict_types=1);

namespace JSONRPC;

/**
 * Interface ProcedureException
 *
 * Base interface for user defined exceptions
 *
 * ==== WARNING! ====
 * This exception interface should be thrown if you want to catch that error by dispatcher and propagate to response,
 * other exception will not be handled and passes up.
 * ==================
 *
 * @package JSONRPC
 */
interface ProcedureException extends \Throwable
{
    public function getData(): mixed;
}
